# ![](git_icon.png =30x30) Introduction git

#### *"Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency."* 

&nbsp;&nbsp;&nbsp; -- http://git-scm.com

__Motivation & Requirements__

Before we start with the introduction to git, we first want to emphasize some general requirements and their motivation why a version control system is needed at all.

1. (large) software projects are tackled by multiple developers
   -> requires __easy exchange__ with other developers
2. keep track of workflow (workflow optimization, error source analysis & minimization)
	-> requires __annotated work history__
3. comparison with older versions (bug localization)
	-> requires easy way to __undo changes__ and __compare code__ to previous project versions
4. focus should lie on software development, so the time needed for repository maintainance & code publication should be minimal
	-> requires easy workflow to __(re)publish code__
	-> requires __easy to use and lightweight__ interface

__Aims__

This introduction is by no means complete, rather it should give the reader a really quick introduction in order to archive the requirements stated before. We give a general overview of the concepts and vocabulary and how to actually use git in practice. For an introduction on how to exchange or publish your code please take a look at [Introduction to Gitlab](../README.md). Special cases are intentionally excluded since git has a very complete documentation and there exist lot of other tutorials that already cover these in great detail. To mention some of them you find a list of tutorials that we find valuable below. Note that they overlap in many points and differ a lot in their scope.

http://stackoverflow.com/questions/315911/git-for-beginners-the-definitive-practical-guide/1350157#1350157

http://blog.osteele.com/posts/2008/05/my-git-workflow/

http://rogerdudler.github.io/git-guide/

http://learnxinyminutes.com/docs/git/

http://gitref.org

Note that there also exist cheat sheets, that contain the most used commands.

https://training.github.com/kit/downloads/github-git-cheat-sheet.pdf



## Concepts

### Branching

Branches represent a separate line of development. The default branch in git is the `master` branch which is created on initialization. On creation a new branch inherits the history of your current branch (e.g. the `master` branch). Then new commits to this branch are recorded in the history of the newly created branch and are independant of the original branch. The motivation behind this is that multiple features or bugfixes are not always developed sequentially but in parallel.

see [Git Branching - What a Branch Is](https://git-scm.com/book/en/v1/Git-Branching-What-a-Branch-Is)

see [Git Branching - Branches in a Nutshell](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)

### Merging

After the development in a branch has finished it is most likely desired to merge your changes back to another branch (e.g. your master branch). By this the history and index of the two branches is combined again.

Note: While merging as a concept is quite simple there exist a few situations where things can get more complicated (e.g. if the original branch has evolved since you branched). 

See [Git Branching - Basic Branching and Merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

### Data stores

![](git-transport.png)

__Workspace__

The workspace (also called working tree or working directory) is the state of files and directories you are currently working on. In other words this is everything that you can open and edit directly.

__Index__

The index or staging area is something that does not exist in traditional VCS. It's basically a layer between your working tree and local repository that gives you control over what files you want to include in your next commit.

__Local repository__

The local repository contains all the data (commits, history, branches) of the repository.

__Remote repository__

A remote repository or simply remote is an instance of the local repository that is hosted on a remote location, e.g. the internet. The default remote in git is called `origin` and is automatically added when you clone from a remote repository. Gits `push` and `fetch` commands provide methods to exchange commits from your local repository with the remote repository.

## Getting started

Even though git works in principle without any configuration, it is recommended to define the authors name & email address for the current user, such that all commit messages are issued with this information.

	git config --global user.name <name>
	git config --global user.email <email>

### Installation

__Mac OS X__

	brew install git

__Debian based (ubuntu)__

	sudo apt-get install git

__Redhat based (centos, fedora)__

	yum install git

### Setting up a repository

In this basic example we create a new repository that contains just a single README file. This README is edited multiple times in different commits and we show how to fetch information about your repo's history and state. 

Note: If you are the only one working on the repo this is sufficient to maintain an annotated work history and the possibility to compare with previous versions.

```bash
# create a folder for our new repository
mkdir my-awesome-repo
cd my-awesome-repo

# initialize the repository
git init

# write a readme file
touch README
echo "welcome to my awesome repository" >> README

# add readme to the repository index
git add README

# commit your newly created file
git commit -m "Added README with greetings to the reader"

# add contact information to the repo
echo "contact: my-awesome-repo@example.com" >> README

# show difference between working directory & head
git diff

# add readme to the repository index
git add README

# show difference between index & head
git diff HEAD

# commit our changes
git commit -m "Added contact information"

# show commit history (press `q` to exit)
git log
```

## Branches

__Creation of a new branch__

	# create branch some_branch
	git branch some_branch

__Switching to a different branch__

	# switch to branch some_branch
	git checkout some_branch

__Merging with the current branch__

	# merge some_branch into the current branch
	#  by combining the history of both branches
	git merge some_branch

__Checkout of remote branches__

	# fetch data from origin
	git fetch origin

	# checkout local_branch from origin/local_branch
	git checkout local_branch

## Working with remote repositories

__Clone remote repository__

	# clone remote repository at 
	#  gitlab.math.ethz.ch:some_namespace/some_project 
	#  into a newly created local folder some_project
	git clone git@gitlab.math.ethz.ch:some_namespace/some_project.git

__Fetching from a remote repository__

	# fetch/update local copy of the remote repository 
	#  (e.g. local copy of the remote branches)
	# note: this does not change your local branches
	git fetch

__Pulling from a remote repository__

	# fetch local copy of the remote repository and merge
	#  the remote branch corresponding to your current (local)
	#  branch into the current branch
	# note: this is comparable with svn update
	git pull

__Pushing local changes to a remote reposiory__

	# push/upload changes in your current branch to the 
	#  remote `origin
	# note: this is comparable with svn commit
	git push

	# push by explicitly specifying the remote to push to (origin)
	#  and the remote branch
	# note: this is useful if the branch was created locally and has 
	#  not been pushed yet (to allow usage of git push without parameters 
	#  you can add the -u flag which connects the local branch with 
	#  the remote branch)
	git push origin some_branch

## Common use cases

### Working on a small subset of files

	# create a new branch
	git branch some_branch
	git checkout some_branch

	# do some changes and commit them
	# ...

	# update your current branch
	#  (only needed when you are working with other people on this branch)
	git pull

	# incoorperate changes from the master branch 
	#  by fetching the changes and merging them into the current branch
	git pull origin master

### Keeping things private

*To be written*

## Command reference

This section contains a very brief overview over the most common commands of git. Further details can be found in the man pages that can be accessed via

	git help <command>

or in the [online reference](https://git-scm.com/docs)

__git init__

*Initializes a new repository*

Takes an optional argument which specifies the directory where the repo should be initialized. If no argument is given the repo is initialized in the current directory.

	git init [<directory>]

 Note: You can either convert an+ unversioned project to git or initialize a new repository depending on the folder where you initialize the repo.

__git clone__

*Clones an already existent repo into a newly created directory*

As the name already suggests, you will receive a full copy of the original repo that contains all branches and the complete history. The source from where the repo is cloned can be either a file path to an existing repo or a remote url (supported protocols are ssh and https).

	git clone <repository> [<directory>]

__git checkout__

	git checkout <branch>

	git checkout <commit>

__git add__

Add files in the working directory to the index.

	git add [<files>...]

__git branch__

List, create, delete a branch from the current branch.

	git branch [--list] [--delete] <name>

__git log__

Show recent commit history

	git log

__git rm__

Remove files from the working tree and index.

	git rm <files>...

__git mv__

Move a file from one place to another while retaining the files history

	git mv <src> <dst>

__git status__

Display differences between the index and the current HEAD commit

	git status