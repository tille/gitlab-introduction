# Introduction to Gitlab

__Abstract__ A general introduction to the most important features of Gitlab is presented. As prerequisite you should have basic knowledge of [Git](https://git-scm.com/) ([short tutorial](git/README.md), [documentation](https://git-scm.com/docs), [book](https://git-scm.com/book)).


__Introduction__ Gitlab is a web based repository manager, based on git with code review, wiki, issue tracking, snippets and continuous integration features.

For all people from SAM the D-Math's Gitlab can be accessed with your regular NETHZ account at https://gitlab.math.ethz.ch/

*Note* that access via ssh is restricted to the ETH Network, so you have to enable VPN (even if you are using the ETH Wifi) or use a network port at ETH.

If you do not have access yet, or have problems to login, please contact --Contact missing-- and ask for permission.

__Links__

[Gitlab user documentation](http://doc.gitlab.com/ce/)

[Gitlab flavoured markdown documentation](http://doc.gitlab.com/ce/markdown/markdown.html)

[Gitlab special references](http://doc.gitlab.com/ce/markdown/markdown.html#special-gitlab-references)

[How To Use the GitLab User Interface To Manage Projects](https://www.digitalocean.com/community/tutorials/how-to-use-the-gitlab-user-interface-to-manage-projects) (somewhat outdated)


## Set up your account

After your first login you should see something similar to this:

![](welcome_screen.png)

Now the first step is to setup your account. That is add your ssh keys and adding some additional profile information.

__Set up ssh keys__

Gitlab allows pushing and pulling via [https](https://de.wikipedia.org/wiki/Hypertext_Transfer_Protocol_Secure) or [ssh](https://de.wikipedia.org/wiki/Secure_Shell). For security reasons and convenience it is recommended to use [ssh](https://de.wikipedia.org/wiki/Secure_Shell).

*NOTE* Do not add ssh keys from machines you do not trust. If you want to give read access to a person, that has no account, give him a local copy of your repository, clone the repository via https (for https you need to sign in on every pull or push operation) or make your repository public (make sure you comply with the license).

1. check for existing keys
	
	```
	$ ls -al ~/.ssh
	```
	
	If you see an existing pair of keys (e.g. `id_rsa` and `id_rsa.pub`) you can just use them and skip step 2.

2. Generate a new key 

	```bash
	$ ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
	```

3. Copy you're __PUBLIC__ key from the machine you want to push/pull from (e.g. `~/.ssh/id_rsa.pub`)
4. Navigate to https://gitlab.math.ethz.ch/profile/keys/new and paste your key (make sure you did not include additional newline characters).

	Note that the title box should be automatically filled with your email address (from the end of your key).

__Setup profile__

The main advantages of gitlab are its many collaboration features. So it's a good idea to add some additional information about your self (at least your full name should be added) to your profile. This can be done at https://gitlab.math.ethz.ch/profile

## Creating a new project

- Navigate to the dashboard

![](create_proj.png)

- Press new project 

![](create_proj2.png)

- Add project information

![](create_proj3.png)

- `Project path` Name of your project / repository. Together with the namespace this forms the path where your process can be accessed.
- `Namespace` You can create your project either in your default namespace (your username), or a different namespace, like a group where you must have master access. Consider using a namespace of a group, if you work equally with multiple people on one project.
- `Imported project from` direct import from a public repository (can also be archived, e.g. for private copies, by pushing from your local copy)
- `Description` A very brief description of your project (Note that a detailed description should be placed in the README)
- `Visibility Level` Choose who should be able to view your project

![](create_proj4.png)

Now you are able to work with your newly created repository.

For example you can clone your repository via ssh by executing (the link can be found at your projects home page)

	git clone git@gitlab.math.ethz.ch:tille/my-awesome-project.git

Some basic command line instructions on how to work with your repository are given at the bottom of your project (until you add a README)

Beside using your newly created project as a central point to sync your repositories, you can use the Issue Tracker, create Merge Requests, add members (see permissions section) or add a wiki.

## Issue tracker

Gitlab has a built-in Issue tracker, that you can use to communicate with other people, that are working on your project. But do not take the term Issue tracker literally. Beside using this to track issues or bugs that you find in the project, you can use this to assign tasks to different users, create a ticket to request a new feature and many more.

Note: The title and text of an issue can contain markdown syntax. Use gitlab flavoured markdown to mention other users, files, commits you are referring to, related or duplicate tickets (see [Gitlab special references](http://doc.gitlab.com/ce/markdown/markdown.html#special-gitlab-references)).

## Permissions

Since one of the main use cases is to keep your code private or at least let you decide about the permissions, Gitlab has advanced permission functionalities.

__Group & Project wide permission__
https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/permissions/permissions.md

__Protected branches__
http://doc.gitlab.com/ce/workflow/protected_branches.html

Blog article: https://about.gitlab.com/2014/11/26/keeping-your-code-protected/

__Public access__
https://gitlab.math.ethz.ch/help/public_access/public_access

# Example

In this example we create a new project from scratch, add & commit all files to a git repository, publish the code on gitlab, use gitlabs integrated issue tracker to create an issue, create a new branch where we fix the problem and in the end merge back our changes to the master branch. As an example project we choose a numerical integration library that solves the dahlquist equation. Please note that the actual implementation details do not really matter. 

## Creating a local repository

We begin with the creation of our project localy. That is initializing an empty git repository, adding some files and later on commit them to our local repository.

```bash
# initialize empty git repository
git init

# Add README.md
wget https://gitlab.math.ethz.ch/tille/example-project-numerical-integration/raw/09d95ebc27eeb5029b2ebbfd84b21b20ced5b42e/README.md
# ...

# Add and commit
git add README.md
git commit -m "Initial commit"

# Add solve_dahlquist_equation.cpp
wget https://gitlab.math.ethz.ch/tille/example-project-numerical-integration/raw/09d95ebc27eeb5029b2ebbfd84b21b20ced5b42e/solve_dahlquist_equation.cpp

# Show difference between working directory and index
git diff

# Add to the index
git add solve_dahlquist_equation.cpp

# Show diff between index
git diff HEAD

# Commit
git commit -m "Added implementation"
```

## Pushing to a remote repository

Now we want to publish our project. In order to do so we create a new project in gitlab

- Navigate to the dashboard

![](create_proj.png)

- Press new project 

![](create_proj2.png)

- Add project information

![](create_proj3.png)

- `Project path` Name of your project / repository. Together with the namespace this forms the path where your process can be accessed.
- `Namespace` You can create your project either in your default namespace (your username), or a different namespace, like a group where you must have master access. Consider using a namespace of a group, if you work equally with multiple people on one project.
- `Imported project from` direct import from a public repository (can also be archived, e.g. for private copies, by pushing from your local copy)
- `Description` A very brief description of your project (Note that a detailed description should be placed in the README)
- `Visibility Level` Choose who should be able to view your project

![](create_proj4.png)

Now we want to upload the code we have written so far.

	# add a remote named origin that points to our project on gitlab
	git remote add origin git@gitlab.math.ethz.ch:tille/example-project-numerical-integration.git

	# push from the current branch to the remote master branch
	git push origin master

## Using the issue tracker

After we uploaded our project we or a user of your library notices that there are same flaws in the implementation. In our case the integration routing will give useless results for certain parameters. Instead of just fixing the problem we first create an issue that documents the bug.

- Navigate to the isse tracker in the sidebar of the project and click new issue
	
	![](issue_example_1.png)

- We will now add some information meanful information. That is

	- `title` - short description of the problem
	- `description` - how the reproduce the problem
	- `assigne` - who should handle this (optional)
	- `milestone`
	- `label` - e.g. category that the issue belongs to

	![](issue_example_2.png)

- Click submit issue

	![](issue_example_3.png)

- After submission people can now reply and discuss on the issue.

	![](issue_example_4.png)

- Since we are the only one working on the project there will be no real discussion. Nevertheless we can still reply to the issue with some further information on the origin of the problem. In our example we found a condition that has to be satisfied such that our routine works as expected, so we will just add this to the current issue.

	![](issue_example_5.png)

## Merging

We know want to fix the issue we have encoutered in the previous section. In our example this means implementation of a different integration routine. We could now just stay in the master branch and do all of the work there with the commands and tools we have just used in the previous section. But since this is a new feature that might consist out of multiple commits and will possibly break your existing code we choose to create a seperate branch and continue our work there. Note that this becomes especially important if there are multiple people working on a project.

	# create a new branch named implicit_euler
	git branch implicit_euler

	# checkout the newly created branch  
	#  (make sure your working directory is clean,  
	#   e.g. by using git stash)  
	git checkout implicit_euler

	# make some changes
	#  add implemenation of the implicit euler method
	# ...

	# commit the changes
	git add solve_dahlquist_equation.cpp
	git commit -m "Added implementation of the implicit euler method \n Fixes #1"

	# make some changes
	#  update the readme
	# ...

	# push changes to the remote repository
	git push origin implicit_euer

After development has finished we want to merge back our changes into the `master` branch. This can be done either using `git merge` or directly in Gitlab by creating a merge request. The advantage of using Gitlab for this is that another member of the project can first review your changes and leave some comments.

__Merge using gits `merge` command__

	# checkout master branch
	git checkout master

	# merge the implicit euler branch into the current branch (master)
	git merge implicit_euler

__Merge by creating a Merge Request in gitlab__

- Navigate to the project and click on `Merge Requests` in the sidebar on the left

	![](merge_request_1.png)

- Click new merge request

- Select the branch to merge from and where to merge it. 

	Note that you can also specify the project from which you want to merge. If you fork the original repository (by pressing fork on the project home page in gitlab) and make changes there you can also create a merge request to incorparate the changes back to the original repository. This has the benefit that you don't even need Permissions to push to Original repository.

	![](merge_request_2.png)

- Press Compare branches

	![](merge_request_3.png)

- Enter some information

	- `Title` - e.g. short description of the feature that was implemented in the original branch
	- `Description` - description of all changes; e.g. backward incompatible changes
	- `Assign to` - who should review the code and do the actual merge. If you first only want to discuss your changes leave this empty and just mention your colleges in your description such that they get notified.
	- `Milestone`
	- `Labels` - e.g. category that the changes belong to

- Click Submit new merge request

<!--__Handeling merge conflicts__

Consider someone changed something in the `solve_dahlquist_equation.cpp` file after you have created your branch where you also made changes to this file. Then as soon as you try to merge the two branches git will most likely complain since it does not know how to resolve the conflict. The same will happen if you create a merge request in gitlab. It will deny the acceptance of the merge request and asks you to resolve the conflict e.g. by merging manuelly. You can then either follow the instructions they provide our use a different way that we propose. We recommend merging the destination branch into the source branch-->

[GitLab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/)

Note: Github calls this feature Pull requests